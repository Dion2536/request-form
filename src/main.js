import Vue from 'vue'
import * as VueGoogleMaps from "vue2-google-maps";
import store from './store';
import VueLodash from 'vue-lodash';
import Antd from 'ant-design-vue';
// import 'ant-design-vue/dist/antd.css';
import './assets/css/ant-theme-custrom.css';
import "./assets/css/app.css";

const options = {
  name: 'lodash'
};
import App from './App'
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCZCdks7BLVBzTE4LKDQhkGFvY90Rm5WPY",
    libraries: "places" // necessary for places input
  }
});
Vue.use(VueLodash, options);
Vue.config.productionTip = false
Vue.use(Antd)

new Vue({
  store,
  render: h => h(App)

}).$mount('#app')