import Vue from 'vue'
import Vuex from 'vuex'

// Make vue aware of Vuex
Vue.use(Vuex);

// the app starts up
export default new Vuex.Store({
    state: {
        ip_serve: 'http://103.80.51.21:3333',
        setFrom: 'พพ',
        formStep1: [],
        formStep2: [],
        formStep3: [],
        formStep1Send: []
    },
    // methods store
    mutations: {
        // set state 
        formStep1State(state, items) {
            localStorage.setItem('formStep1', JSON.stringify(items));
            state.formStep1 = items;
        },
        formStep1SendState(state, items) {
            localStorage.setItem('formStep1Send', JSON.stringify(items));
            state.formStep1Send = items;
        },
        formStep2State(state, items) {
            localStorage.setItem('formStep2', JSON.stringify(items));
            state.formStep2 = items;
        },
        formStep3State(state, items) {
            localStorage.setItem('formStep3', JSON.stringify(items));
            state.formStep3 = items;
        },
        // eslint-disable-next-line no-unused-vars
        clearLocal: state => {
            if (localStorage.getItem('formStep1') !== null) localStorage.removeItem('formStep1');
            if (localStorage.getItem('formStep2') !== null) localStorage.removeItem('formStep2');
            if (localStorage.getItem('formStep3') !== null) localStorage.removeItem('formStep3');
            if (localStorage.getItem('formStep1Send') !== null) localStorage.removeItem('formStep1Send');
        }
    },
    getters: {
        // eslint-disable-next-line no-unused-vars
        getformStep1: state => {
            return JSON.parse(localStorage.getItem('formStep1'));
        },
        getIpServe: state => state.ip_serve,
        getFrom: state => state.setFrom

    }
})